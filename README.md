# Installation

cf **INSTALL.md**

# Utilisation

Pour utilise la lib, importer avec `import LibGPIO`

S'il y a un problème à l'importation, ne pas oublier d'installer la lib

Pour utiliser les gpios, instanciez une la classe gpio3

`readInput(pin)`: permet de lire la valeur sur le pin, renvoie 1 si c'est high, 0 si low.

`readInputs()`:permet de lire tous les ports GPIO et renvoie une chaîne de caractère de la forme `0100011...` 

`setOutput(pin,val)`: permet de fixer une valeur val pour un pin, renvoie le numero de pin si ça c'est bien passé

`setPWM(pin, val)`: donne une valeur à un pin qui supporte le PWM, pin le numero de pin, val = [0,99]

`unSetPWM(pin)`: stop et supprime un pwm

`lastFunctionPin(pin)`: donne la dernière utilisation d'un pin (in/out), je me suis dit que ça pouvait être utile

**Ne pas oublier de `del` l'objet gpio3 après utilisation pour cleanup les pins !**

**Attention pour tester, ne pas oublier de metter un sleep pour voir les effets**


# Le Hardware

**On utilise les ports BCM**

![le dessin des pins GPIO](gpio-map.png)