#!/usr/bin/python3

try:
    import RPi.GPIO as GPIO
except RuntimeError:
    print("Erreur d'importation de RPi.GPIO!")    

"""
TODO
gerer les exceptions
toujours quelques bugs avec le pwm
"""

class gpio3:
    """ classe pour gerer facilement les GPIOs """
    pwm12 = None
    pwm13 = None

    def __init__(self):
        """ 
        initialisation du board GPIO 
        /!\\ On utilise les pins BCM et non BOARD
        """
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False) #Puisque l'on va utiliser plusieurs pins pour differentes utilisations au cours du temps, l'on desactive les warnings
        #channel = [2,27] # Definir la plage des pins BCM que l'on peut utiliser
        return

    def readInput(self, pin):
        """ 
        lecture d'un seul pin 
        renvoie la valeur d'un pin 1/0
        """
        GPIO.setup(pin, GPIO.IN)
        return 1 if GPIO.input(pin) == GPIO.HIGH else 0
    
    def readInputs(self):
        """ 
        lecture de tous les pins 
        retourne une chaine de caractere avec 1 si HIGH, 0 si LOW de tous les pins d'un channel
        """
        val_retour = 2
        for i in range(2,27):
            x = self.readInput(i)
            val_retour *= 10
            val_retour += x
        val_retour = str(val_retour)
        val_retour = val_retour[1:]
        return val_retour


    def setOutput(self, pin, val):
        """ ecriture d'une valeur sur un pin """
        GPIO.setup(pin, GPIO.OUT)
        GPIO.output(pin, GPIO.HIGH) if val > 0 else GPIO.output(pin, GPIO.LOW)
        return pin
    
    def setPWM(self, pin, val):
        """ 
        /!\\ EXPERIMENTAL
        permet de donner une valeur a un pin en pwm
        val entre 0 et 100
        Si l'on n'a pas encore cree d'objet pwm sur ce pin, ne rien metttre pour l'argument pwm
        retourne un objet pwm que l'on peut manipuler
        """
        if pin != (12 or 13):
            print("erreur de port, ceux ne sont pas des ports pwm")
            return
        elif pin == 12:
            if(gpio3.pwm12 == None):
                GPIO.setmode(pin, GPIO.OUT)
                gpio3.pwm12 = GPIO.PWM(pin,50) #50 Hz
                gpio3.pwm12.start(val)
                
            else:
                gpio3.pwm12.ChangeDutyCycle(val)

        elif pin == 13:
            if(gpio3.pwm13 == None):
                GPIO.setmode(pin, GPIO.OUT)
                gpio3.pwm13 = GPIO.PWM(pin,50) #50 Hz
                gpio3.pwm13.start(val)
            else:
                gpio3.pwm12.ChangeDutyCycle(val)
        return 

    def unSetPWM(self, pin):
        """ arret d'un port pwm"""
        if pin == 12:
            gpio3.pwm12.stop()
            gpio3.pwm12 = None
        elif pin == 13:
            gpio3.pwm13.stop()
            gpio3.pwm13 = None
        else:
            print("erreur de port, ceux ne sont pas des ports pwm")
        return
    
    def lastFunctionPin(self, pin):
        """renvoie la derniere utilite d'un pin"""
        return GPIO.gpio_function(pin)

    def __del__(self):
        GPIO.cleanup()