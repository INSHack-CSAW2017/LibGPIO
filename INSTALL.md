## Si Raspbian est à jour :

* Pas Besoin de faire quoique se soit

## Sinon ou pas sûr:

`$ python3`

`> import RPi.GPIO as GPIO`

`> GPIO.VERSION`

la dernière version est la **0.6.3**

## Installation

`$ sudo apt-get install python-rpi.gpio python3-rpi.gpio`
